<?php

namespace Superatom\Routing;

use Symfony\Component\HttpFoundation\RedirectResponse;

class Redirector
{
    /**
     * @var Router
     */
    protected $router;

    public function __construct(Router $router)
    {
        $this->router = $router;
    }

    /**
     * @param string $uri
     * @param int    $status
     * @param array  $headers
     *
     * @return RedirectResponse
     */
    public function to($uri, $status = 302, $headers = [])
    {
        return new RedirectResponse($uri, $status, $headers);
    }

    /**
     * @param string $name
     * @param array  $params
     * @param int    $status
     * @param array  $headers
     *
     * @return RedirectResponse
     */
    public function route($name, $params = [], $status = 302, $headers = [])
    {
        $path = $this->router->pathFor($name, $params);

        return $this->to($path, $status, $headers);
    }
}
