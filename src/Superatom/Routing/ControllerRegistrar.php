<?php

namespace Superatom\Routing;

use ReflectionClass;
use ReflectionMethod;

class ControllerRegistrar extends Router
{
    /**
     * HTTP method verbs for controller routes.
     *
     * @var array
     */
    protected $verbs = ['get', 'head', 'post', 'put', 'delete', 'options'];

    /**
     * HTTP verbs and URI mapping for Resource controller methods.
     *
     * Format
     *   [controller_method => [verb, uri], ...]
     *
     * @var array
     */
    protected $resources = [
        'index' => ['get',    ''],
        'create' => ['get',    '/create'],
        'store' => ['post',   ''],
        'show' => ['get',    '/{id:i}'],
        'edit' => ['get',    '/{id:i}/edit'],
        'update' => ['put',    '/{id:i}'],
        'destroy' => ['delete', '/{id:i}'],
    ];

    /**
     * Register routes to controller methods.
     *
     * @param string $class_name
     * @param array  $names
     *
     * @return RouteGroup
     */
    public function controller($class_name, $names = [])
    {
        $controller = new ReflectionClass($class_name);
        $routable = $this->getControllerRoutable($controller, $names);

        return $this->mapRoutable($controller, $routable);
    }

    /**
     * Register routes to resource actions.
     *
     * @param string $class_name
     *
     * @return RouteGroup
     */
    public function resource($class_name)
    {
        $controller = new ReflectionClass($class_name);
        $routable = $this->getResourceRoutable($controller);

        return $this->mapRoutable($controller, $routable);
    }

    /**
     * return prefix from controller name.
     *
     * @param string $controller
     *
     * @return string
     */
    public function getPrefix($controller)
    {
        $class = last(explode('\\', $controller));

        return '/'.str_replace('_controller', '', snake_case($class));
    }

    /**
     * Get the Controller route rules.
     *
     * @param ReflectionClass $controller
     * @param array           $names
     *
     * @return array
     */
    public function getControllerRoutable($controller, $names)
    {
        $routes = [];
        foreach ($controller->getMethods(ReflectionMethod::IS_PUBLIC) as $method) {
            if ($this->isControllerRoutable($method)) {
                $data = $this->getControllerMethodData($method, $names);
                $routes[$method->name][] = $data;
            }
        }

        return $routes;
    }

    /**
     * Get the Resource route rules.
     *
     * @param ReflectionClass $controller
     *
     * @return array
     */
    public function getResourceRoutable($controller)
    {
        $routes = [];
        $groups = str_replace('/', '.', ltrim($this->processGroups(), '/'));
        $prefix = ltrim($this->getPrefix($controller->name), '/');
        foreach ($this->getResourceMethods($controller) as $method) {
            list($verb, $uri) = $this->resources[$method->name];
            $name = implode('.', [$groups, $prefix, $method->name]);
            $routes[$method->name][] = [$verb, $uri, ltrim($name, '.')];
        }

        return $routes;
    }

    public function isAbstractController($class)
    {
        return ($class === 'Superatom\Controller');
    }

    /**
     * Map routable methods to router handlers.
     *
     * @param ReflectionClass $controller
     * @param array           $routable
     *
     * @return RouteGroup
     */
    protected function mapRoutable($controller, array $routable)
    {
        $prefix = $this->getPrefix($controller->name);

        return $this->group($prefix, function () use ($controller, $routable) {
            foreach ($routable as $method => $routes) {
                foreach ($routes as $route) {
                    list($verb, $uri, $name) = $route;
                    $handler = $controller->name.'@'.$method;
                    $this->$verb($uri, $handler)->setName($name);
                }
            }
        });
    }

    /**
     * Determine if route handling by routable controller.
     *
     * @param ReflectionMethod $method
     *
     * @return bool
     */
    protected function isControllerRoutable(ReflectionMethod $method)
    {
        if ($this->isAbstractController($method->class)) {
            return false;
        }

        return starts_with($method->name, $this->verbs);
    }

    /**
     * Get route data for routable controller method.
     *
     * @param ReflectionMethod $method
     * @param array            $names
     *
     * @return array
     */
    protected function getControllerMethodData(ReflectionMethod $method, $names = [])
    {
        $segments = explode('_', snake_case($method->name));
        $verb = array_shift($segments);
        $params = $this->getParameterString($method);
        $uri = '/'.implode('/', [implode('-', $segments), $params]);
        $uri = rtrim($uri, '/');
        $name = (isset($names[$method->name])) ? $names[$method->name] : null;

        return [$verb, $uri, $name];
    }

    /**
     * Get valid resource methods in the correct order.
     *
     * @param ReflectionClass $controller
     *
     * @return ReflectionMethod[]
     */
    protected function getResourceMethods(ReflectionClass $controller)
    {
        $resourceMethods = [];
        foreach (array_keys($this->resources) as $methodName) {
            if ($controller->hasMethod($methodName)) {
                $method = $controller->getMethod($methodName);
                if ($method->isPublic()) {
                    $resourceMethods[] = $method;
                }
            }
        }

        return $resourceMethods;
    }

    /**
     * Get parameter string for routable controller method.
     *
     * @param ReflectionMethod $method
     *
     * @return string
     */
    protected function getParameterString(ReflectionMethod $method)
    {
        $params = '';
        foreach ($method->getParameters() as $param) {
            if ($param->isOptional()) {
                $tmp = '[/{'.$param->name.'}]';
            } else {
                $tmp = '/{'.$param->name.'}';
            }
            $params .= $tmp;
        }

        return ltrim($params, '/');
    }
}
