<?php

namespace Superatom\Routing;

use Superatom\Application;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class ControllerResolver
{
    /**
     * @var Application
     */
    protected $app;

    public function __construct(Application $app)
    {
        $this->app = $app;
    }

    /**
     * Handle matched route.
     *
     * @param mixed    $handler
     * @param Request  $req
     * @param Response $res
     *
     * @return mixed
     */
    public function __invoke($handler, Request $req, Response $res)
    {
        if (is_string($handler)) {
            list($class, $method) = explode('@', $handler);
            $controller = ControllerFactory::create($class);
            $controller->setApplication($this->app)
                ->setRequest($req)
                ->setResponse($res);
            $handler = [$controller, $method];
        }

        if (isset($controller)) {
            call_user_func([$controller, 'before']);
        }
        $response = call_user_func_array($handler, $req->attributes->all());
        if (isset($controller)) {
            call_user_func([$controller, 'after']);
        }

        return $response;
    }
}
