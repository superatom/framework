<?php

namespace Superatom\Routing;

use Superatom\Application;
use Superatom\Input;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

abstract class Controller
{
    /**
     * Application instance.
     *
     * @var Application
     */
    protected $app;

    /**
     * Symfony Request instance.
     *
     * @var Request
     */
    protected $request;

    /**
     * Symfony Response instance.
     *
     * @var Response
     */
    protected $response;

    /**
     * Input instance.
     *
     * @var Input
     */
    protected $input;

    /**
     * @param $app
     *
     * @return $this
     */
    public function setApplication($app)
    {
        $this->app = $app;

        return $this;
    }

    /**
     * @param Request $req
     *
     * @return $this
     */
    public function setRequest(Request $req)
    {
        $this->request = $req;

        $this->input = new Input($req);

        return $this;
    }

    /**
     * @param Response $res
     *
     * @return $this
     */
    public function setResponse(Response $res)
    {
        $this->response = $res;

        return $this;
    }

    public function before()
    {
        // do nothing.
    }

    public function after()
    {
        // do nothing.
    }

    /**
     * View render method shortcut.
     *
     * @param array  $data
     * @param string $template
     *
     * @return string
     */
    public function render($data = [], $template = null)
    {
        if (is_null($template)) {
            $route = $this->app->router->getCurrentRoute();
            $template = str_replace('.', '/', $route->getName());
        }

        return $this->app->view->render($template, $data);
    }
}
