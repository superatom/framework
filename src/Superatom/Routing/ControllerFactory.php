<?php

namespace Superatom\Routing;

class ControllerFactory
{
    /**
     * @param string $controller_name
     *
     * @return Controller
     */
    public static function create($controller_name)
    {
        $controller = new \ReflectionClass($controller_name);

        return self::resolve($controller);
    }

    protected static function resolve(\ReflectionClass $class)
    {
        $constructor = $class->getConstructor();
        if (is_null($constructor)) {
            return $class->newInstance();
        }

        $dependencies = self::resolveParams($constructor->getParameters());

        return $class->newInstanceArgs($dependencies);
    }

    /**
     * @param \ReflectionParameter[] $params
     * @return array
     */
    protected static function resolveParams(array $params)
    {
        $dependencies = [];
        foreach ($params as $param) {
            if ($param->isDefaultValueAvailable()) {
                $dependencies[] = $param->getDefaultValue();
            } elseif ($param->getClass()) {
                $dependencies[] = self::resolve($param->getClass());
            } else {
                $mes = 'Could not resolve parameter: ' . $param->getDeclaringClass()->name . ' $' . $param->name;
                throw new \InvalidArgumentException($mes);
            }
        }

        return $dependencies;
    }
}
