<?php

namespace Superatom;

use Symfony\Component\Console\Output\ConsoleOutput;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\HttpExceptionInterface;
use Symfony\Component\Console\Application as SymfonyApplication;

class ErrorHandler
{
    /**
     * @var callable
     */
    protected $errorHandler;

    /**
     * @var callable
     */
    protected $notFoundHandler;

    /**
     * @var Application
     */
    protected $app;

    public function __construct(Application $app)
    {
        $this->app = $app;
        $this->errorHandler = $this->defaultErrorHandler();
        $this->notFoundHandler = $this->defaultNotFoundHandler();
    }

    public function register()
    {
        set_error_handler([$this, 'handleError']);
        set_exception_handler([$this, 'handleUncaughtException']);
        register_shutdown_function([$this, 'handleShutdown']);
    }

    public function error(callable $handler)
    {
        $this->errorHandler = $handler;
    }

    public function notFound(callable $handler)
    {
        $this->notFoundHandler = $handler;
    }

    public function handleError($level, $message, $file, $line)
    {
        if (error_reporting() & $level) {
            throw new \ErrorException($message, 0, $level, $file, $line);
        }
    }

    /**
     * Determine if the error type is Fatal.
     *
     * @param int $type
     *
     * @return bool
     */
    public static function isFatal($type)
    {
        return in_array($type, [E_ERROR, E_CORE_ERROR, E_COMPILE_ERROR, E_PARSE]);
    }

    public function handleUncaughtException($e)
    {
        $this->handleException($e);
    }

    public function handleShutdown()
    {
        $err = error_get_last();
        if (!is_null($err)) {
            if (!self::isFatal($err['type'])) {
                return;
            }
            $ex = new \ErrorException($err['message'], $err['type'], 0, $err['file'], $err['line']);
            $this->handleException($ex);
        }
    }

    public function handleException(\Exception $e)
    {
        if ($this->runningInConsole()) {
            $this->displayConsoleError($e);
        } else {
            $this->displayHttpError($e);
        }
    }

    /**
     * Determine if we are running in the console.
     *
     * @return bool
     */
    public function runningInConsole()
    {
        return php_sapi_name() == 'cli';
    }

    protected function displayConsoleError(\Exception $e)
    {
        (new SymfonyApplication())->renderException($e, new ConsoleOutput());
    }

    protected function displayHttpError(\Exception $e)
    {
        $status = 500;
        $headers = [];
        if ($e instanceof HttpExceptionInterface) {
            $status = $e->getStatusCode();
            $headers = $e->getHeaders();
        }

        if (404 === $status) {
            $body = call_user_func($this->notFoundHandler, $e);
        } else {
            $this->app->logger->error($e);
            $body = call_user_func($this->errorHandler, $e);
        }

        $response = new Response($body, $status, $headers);
        $response->send();
    }

    protected function defaultErrorHandler()
    {
        return function (\Exception $e) {
            return 'whoops, looks like something went wrong.';
        };
    }

    protected function defaultNotFoundHandler()
    {
        return function (\Exception $e) {
            return '404 Not found.';
        };
    }
}
