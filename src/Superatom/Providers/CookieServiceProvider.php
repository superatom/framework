<?php

namespace Superatom\Providers;

use Illuminate\Cookie\CookieJar;
use Pimple\Container;
use Pimple\ServiceProviderInterface;

class CookieServiceProvider implements ServiceProviderInterface
{
    public function register(Container $app)
    {
        $app['cookie'] = function () {
            return new CookieJar();
        };
    }
}
