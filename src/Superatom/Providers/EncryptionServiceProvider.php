<?php

namespace Superatom\Providers;

use Pimple\Container;
use Pimple\ServiceProviderInterface;
use Superatom\Config\Repository;
use Superatom\Encrypter;

class EncryptionServiceProvider implements ServiceProviderInterface
{
    public function register(Container $app)
    {
        $app['encrypter'] = function () use ($app) {
            /** @var Repository $config */
            $config = $app['config'];
            $key = $config->get('app.key');
            $cipher = $config->get('app.cipher');

            return new Encrypter($key, $cipher);
        };
    }
}
