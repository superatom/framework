<?php

namespace Superatom\Providers;

use Pimple\Container;
use Pimple\ServiceProviderInterface;
use Superatom\ErrorHandler;
use Whoops\Handler\PrettyPageHandler;
use Whoops\Run;

class ExceptionServiceProvider implements ServiceProviderInterface
{
    public function register(Container $app)
    {
        error_reporting(E_ALL);
        $app['errorHandler'] = new ErrorHandler($app);
        $app['errorHandler']->register();

        if ($app->debug() && $app->environment !== 'testing') {
            $this->setupWhoops($app);
        }
    }

    /**
     * @param \Superatom\Application $app
     */
    protected function setupWhoops($app)
    {
        $handler = new PrettyPageHandler();
        $handler->addEditor('idea', function ($file, $line) use ($app) {
            $vm_dir = $app->config->get('whoops.vmPathTranslation.vm');
            $host_dir = $app->config->get('whoops.vmPathTranslation.host');
            if (!empty($vm_dir) && !empty($host_dir)) {
                $file = str_replace($vm_dir, $host_dir, $file);
            }
            $idea_host = $app->config->get('whoops.idea.host', 'localhost');
            $idea_port = $app->config->get('whoops.idea.port', '63342');
            $url = "http://$idea_host:$idea_port/api/file/?file=%file&line=%line";
            $url = str_replace('%line', rawurlencode($line), $url);
            $url = str_replace('%file', rawurlencode($file), $url);

            return ['url' => $url, 'ajax' => true];
        });

        $editor = $app->config->get('whoops.editor');
        if (!is_null($editor)) {
            $handler->setEditor($editor);
        }

        $app->errorHandler->error(function ($e) use ($app, $handler) {
            $whoops = new Run();
            $whoops->allowQuit(false);
            $whoops->writeToOutput(false);
            $whoops->pushHandler($handler);

            return $whoops->handleException($e);
        });
    }
}
