<?php

namespace Superatom\Providers;

use Pimple\Container;
use Pimple\ServiceProviderInterface;
use Superatom\View\Twig as AtomTwig;

class ViewServiceProvider implements ServiceProviderInterface
{
    public function register(Container $app)
    {
        $app['view'] = function () use ($app) {
            $config = $app['config'];
            $twig = new AtomTwig($config->get('app.templates'));

            if ($config->has('twig')) {
                $opts = $config->get('twig');
                if ($app->debug()) {
                    $opts['auto_reload'] = true;
                    $opts['strict_variables'] = true;
                }
                $twig->twigOptions = $opts;
            }

            return $twig;
        };
    }
}
