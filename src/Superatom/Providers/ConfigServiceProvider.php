<?php

namespace Superatom\Providers;

use Pimple\Container;
use Pimple\ServiceProviderInterface;
use Superatom\Config\Repository;

class ConfigServiceProvider implements ServiceProviderInterface
{
    public function register(Container $app)
    {
        $config = new Repository($app['basePath'].'/config');
        $app['config'] = $config;

        $timezone = $config->get('app.timezone', 'UTC');
        date_default_timezone_set($timezone);

        mb_internal_encoding('UTF-8');
    }
}
