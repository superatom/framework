<?php

namespace Superatom\Providers;

use Monolog\ErrorHandler;
use Monolog\Handler\StreamHandler;
use Monolog\Logger;
use Pimple\Container;
use Pimple\ServiceProviderInterface;

class LoggerServiceProvider implements ServiceProviderInterface
{
    public function register(Container $app)
    {
        $app['logger'] = function () use ($app) {
            $logger = new Logger('superatom');
            $output = $app['config']->get('app.log.output');
            $severity = Logger::toMonologLevel($app['config']->get('app.log.severity'));
            $logger->pushHandler(new StreamHandler($output, $severity));
            ErrorHandler::register($logger);

            return $logger;
        };
    }
}
