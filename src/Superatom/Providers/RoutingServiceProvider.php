<?php

namespace Superatom\Providers;

use Pimple\Container;
use Pimple\ServiceProviderInterface;
use Superatom\Routing\ControllerRegistrar;
use Superatom\Routing\ControllerResolver;
use Superatom\Routing\Redirector;

class RoutingServiceProvider implements ServiceProviderInterface
{
    public function register(Container $app)
    {
        $resolver = new ControllerResolver($app);
        $router = new ControllerRegistrar();
        $router->setRouteResolver($resolver);
        $app['router'] = $router;
        $app['redirector'] = new Redirector($router);
    }
}
