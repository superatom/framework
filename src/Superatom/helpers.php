<?php

/**
 * Application functions.
 */
if (!function_exists('app')) {
    /**
     * Return application instance.
     *
     * @param string $service
     *
     * @return mixed|\Superatom\Application
     */
    function app($service = null)
    {
        if ($service) {
            return \Superatom\Application::getInstance()->{$service};
        }

        return \Superatom\Application::getInstance();
    }
}

if (!function_exists('base_path')) {
    /**
     * Return container basePath value.
     *
     * @param string $path
     *
     * @return string
     */
    function base_path($path = null)
    {
        if (is_null($path)) {
            return app()->basePath;
        }

        return app()->basePath.'/'.trim($path, '/');
    }
}

if (!function_exists('storage_path')) {
    /**
     * @param string $path
     *
     * @return string
     */
    function storage_path($path = null)
    {
        return base_path('storage/'.trim($path, '/'));
    }
}

/*
 * Default container getters
 */

if (!function_exists('redirector')) {
    /**
     * @return \Superatom\Routing\Redirector
     */
    function redirector()
    {
        return app()->redirector;
    }
}

if (!function_exists('router')) {
    /**
     * @return \Superatom\Routing\ControllerRegistrar
     */
    function router()
    {
        return app()->router;
    }
}

if (!function_exists('view')) {
    /**
     * @return \Superatom\View\View
     */
    function view()
    {
        return app()->view;
    }
}

/*
 * Routing helper functions
 */

if (!function_exists('delete')) {
    /**
     * @param string $uri
     * @param mixed  $handler
     *
     * @return \Superatom\Routing\Route
     */
    function delete($uri, $handler)
    {
        return router()->delete($uri, $handler);
    }
}

if (!function_exists('get')) {
    /**
     * @param string $uri
     * @param mixed  $handler
     *
     * @return \Superatom\Routing\Route
     */
    function get($uri, $handler)
    {
        return router()->get($uri, $handler);
    }
}

if (!function_exists('post')) {
    /**
     * @param string $uri
     * @param mixed  $handler
     *
     * @return \Superatom\Routing\Route
     */
    function post($uri, $handler)
    {
        return router()->post($uri, $handler);
    }
}

if (!function_exists('put')) {
    /**
     * @param string $uri
     * @param mixed  $handler
     *
     * @return \Superatom\Routing\Route
     */
    function put($uri, $handler)
    {
        return router()->put($uri, $handler);
    }
}

if (!function_exists('controller')) {
    /**
     * @param string $controller
     * @param array  $names
     *
     * @return \Superatom\Routing\RouteGroup
     */
    function controller($controller, array $names = [])
    {
        return router()->controller($controller, $names);
    }
}

if (!function_exists('resource')) {
    /**
     * @param string $controller
     *
     * @return \Superatom\Routing\RouteGroup
     */
    function resource($controller)
    {
        return router()->resource($controller);
    }
}
