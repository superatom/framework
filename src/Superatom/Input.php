<?php

namespace Superatom;

use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\Request;

class Input
{
    /**
     * @var Request
     */
    protected $request;

    public function __construct(Request $request)
    {
        $this->request = $request;
    }

    /**
     * Get all request items.
     *
     * @return array
     */
    public function all()
    {
        $req = $this->request;

        return array_replace_recursive($req->query->all(), $req->request->all(), $req->files->all());
    }

    /**
     * @param string $key
     * @param null   $default
     *
     * @return UploadedFile
     */
    public function file($key, $default = null)
    {
        return $this->request->files->get($key, $default, $deep = true);
    }

    /**
     * Retrieve an input item from request.
     *
     * @param string $key
     * @param string $default
     *
     * @return mixed
     */
    public function get($key, $default = null)
    {
        return array_get($this->all(), $key, $default);
    }

    /**
     * Get a subset of the items from the input data.
     *
     * @param array $keys
     *
     * @return array
     */
    public function only(...$keys)
    {
        return array_only($this->all(), $keys);
    }

    /**
     * Determine if the request contains key for an input item.
     *
     * @param string $key
     *
     * @return bool
     */
    public function has($key)
    {
        return array_key_exists($key, $this->all());
    }
}
