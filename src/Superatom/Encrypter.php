<?php

namespace Superatom;

use Illuminate\Encryption\Encrypter as IlluminateEncrypter;

class Encrypter
{
    protected $key;
    protected $cipher;

    /**
     * @var IlluminateEncrypter
     */
    protected $encrypter;

    public function __construct($key, $cipher)
    {
        $this->key = $key;
        $this->cipher = $cipher;
    }

    public function encrypt($value)
    {
        return $this->getInstance()->encrypt($value);
    }

    public function decrypt($payload)
    {
        return $this->getInstance()->decrypt($payload);
    }

    /**
     * @return IlluminateEncrypter
     */
    public function getInstance()
    {
        if (is_null($this->encrypter)) {
            $this->encrypter = new IlluminateEncrypter($this->key, $this->cipher);
        }

        return $this->encrypter;
    }
}
