<?php

namespace Superatom;

use DebugBar\DebugBar;
use Illuminate\Cookie\CookieJar;
use Monolog\Logger;
use Pimple\Container;
use Superatom\Config\Repository;
use Superatom\Routing\ControllerRegistrar;
use Superatom\Routing\Redirector;
use Superatom\View\View;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * @property string $basePath
 * @property string $environment
 * @property Repository $config
 * @property CookieJar $cookie
 * @property DebugBar $debugbar
 * @property ErrorHandler $errorHandler
 * @property Logger $logger
 * @property Redirector $redirector
 * @property ControllerRegistrar $router
 * @property View $view
 */
class Application extends Container
{
    const VERSION = '0.0.0';

    /**
     * @var Application
     */
    protected static $instance;

    public function __construct($basePath)
    {
        parent::__construct();

        static::$instance = $this;

        $this->basePath = $basePath;
        $this->environment = env('ATOM_ENV', 'production');

        $this->registerCoreServiceProviders();

        $this->registerServiceProviders((array) $this->config->get('app.providers'));
    }

    /**
     * Get data from container.
     *
     * @param string $key
     *
     * @return mixed
     */
    public function __get($key)
    {
        return $this[$key];
    }

    /**
     * Set data to container.
     *
     * @param string $key
     * @param mixed  $value
     */
    public function __set($key, $value)
    {
        $this[$key] = $value;
    }

    /**
     * Determine if debug mode enabled.
     *
     * @return bool
     */
    public function debug()
    {
        return ($this->config->get('app.debug') === true);
    }

    /**
     * @return Application
     */
    public static function getInstance()
    {
        return static::$instance;
    }

    /**
     * @param Request  $request
     * @param Response $response
     *
     * @return Response
     */
    public function handle(Request $request, Response $response)
    {
        Request::enableHttpMethodParameterOverride();

        $response = $this->router->dispatch($request, $response);

        return $response->prepare($request);
    }

    /**
     * @param Request  $request
     * @param Response $response
     */
    public function run(Request $request = null, Response $response = null)
    {
        if (is_null($request)) {
            $request = Request::createFromGlobals();
        }

        if (is_null($response)) {
            $response = new Response();
        }

        $this->handle($request, $response)->send();
    }

    protected function registerCoreServiceProviders()
    {
        $providers = [
            Providers\ConfigServiceProvider::class,
            Providers\LoggerServiceProvider::class,
            Providers\ExceptionServiceProvider::class,
            Providers\RoutingServiceProvider::class,
            Providers\CookieServiceProvider::class,
        ];

        $this->registerServiceProviders($providers);
    }

    protected function registerServiceProviders(array $providers)
    {
        foreach ($providers as $provider) {
            with(new $provider())->register($this);
        }
    }
}
