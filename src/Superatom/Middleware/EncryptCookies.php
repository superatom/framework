<?php

namespace Superatom\Middleware;

use Illuminate\Contracts\Encryption\DecryptException;
use Superatom\Encrypter;
use Symfony\Component\HttpFoundation\Cookie;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class EncryptCookies
{
    /**
     * @var Encrypter
     */
    protected $encrypter;

    public function __construct(Encrypter $encrypter)
    {
        $this->encrypter = $encrypter;
    }

    public function __invoke(Request $req, Response $res, callable $next)
    {
        return $this->encrypt($next($this->decrypt($req), $res));
    }

    /**
     * @param Request $req
     *
     * @return Request
     */
    protected function decrypt(Request $req)
    {
        foreach ($req->cookies as $key => $val) {
            try {
                $req->cookies->set($key, $this->encrypter->decrypt($val));
            } catch (DecryptException $e) {
                $req->cookies->set($key, null);
            }
        }

        return $req;
    }

    /**
     * @param Response $res
     *
     * @return Response
     */
    protected function encrypt(Response $res)
    {
        foreach ($res->headers->getCookies() as $cookie) {
            /* @var Cookie $cookie */
            $res->headers->setCookie($this->duplicate(
                $cookie, $this->encrypter->encrypt($cookie->getValue())
            ));
        }

        return $res;
    }

    /**
     * Duplicate a cookie with a new value.
     *
     * @param Cookie $c
     * @param mixed  $value
     *
     * @return Cookie
     */
    protected function duplicate(Cookie $c, $value)
    {
        return new Cookie(
            $c->getName(), $value, $c->getExpiresTime(), $c->getPath(),
            $c->getDomain(), $c->isSecure(), $c->isHttpOnly()
        );
    }
}
