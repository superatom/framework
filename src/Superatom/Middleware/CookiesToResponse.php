<?php

namespace Superatom\Middleware;

use Illuminate\Cookie\CookieJar;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class CookiesToResponse
{
    /**
     * @var CookieJar
     */
    protected $cookie;

    public function __construct(CookieJar $cookie)
    {
        $this->cookie = $cookie;
    }

    public function __invoke(Request $req, Response $res, $next)
    {
        /** @var Response $newResponse */
        $newResponse = $next($req, $res);

        foreach ($this->cookie->getQueuedCookies() as $cookie) {
            $newResponse->headers->setCookie($cookie);
        }

        return $newResponse;
    }
}
