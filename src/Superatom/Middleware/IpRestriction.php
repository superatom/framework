<?php

namespace Superatom\Middleware;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class IpRestriction
{
    /**
     * @var array
     */
    protected $ips = ['127.0.0.1', '::1'];

    /**
     * @param string $ip
     */
    public function addIp($ip)
    {
        $this->ips[] = $ip;
    }

    /**
     * @param array $ips
     */
    public function setIps(array $ips)
    {
        $this->ips = $ips;
    }

    public function __invoke(Request $req, Response $res, callable $next)
    {
        $ip = $req->getClientIp();

        if (in_array($ip, $this->ips, $strict = true)) {
            return $next($req, $res);
        }

        return $res->setStatusCode(404);
    }
}
