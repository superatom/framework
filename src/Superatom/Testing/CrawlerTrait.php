<?php

namespace Superatom\Testing;

use Superatom\Application;
use Symfony\Component\DomCrawler\Crawler;
use PHPUnit_Framework_Assert as PHPUnit;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

trait CrawlerTrait
{
    /**
     * The application instance.
     *
     * @var Application
     */
    protected $app;

    /**
     * The last response returned by the application.
     *
     * @var Response
     */
    protected $lastResponse;

    /**
     * The current request instance.
     *
     * @var Request
     */
    protected $currentRequest;

    /**
     * The DomCrawler instance.
     *
     * @var Crawler
     */
    protected $crawler;

    /**
     * The current requested uri.
     *
     * @var string
     */
    protected $currentUri;

    /**
     * The cookie parameters ($_COOKIE).
     *
     * @var array
     */
    protected $cookies = [];

    /**
     * The file parameters ($_FILES).
     *
     * @var array
     */
    protected $files = [];

    /**
     * The server parameters ($_SERVER).
     *
     * @var array
     */
    protected $server = [];

    public function browse($uri = null)
    {
        if ($uri) {
            $this->request($uri);
        }

        $this->currentUri = $this->currentRequest->getUri();

        $this->lastResponse = $this->app->handle($this->currentRequest, new Response());

        $this->clearInputs()->followRedirects();

        $this->crawler = new Crawler($this->lastResponse->getContent(), $this->currentUri);

        return $this;
    }

    public function request($uri, $method = 'GET', $params = [], $content = null)
    {
        $this->currentRequest = Request::create(
            $uri, $method, $params, $this->cookies, $this->files, $this->server, $content);

        return $this;
    }

    public function route($name, $method = 'GET', $routeParams = [], $params = [], $content = null)
    {
        $uri = $this->app->router->pathFor($name, $routeParams);

        return $this->request($uri, $method, $params, $content);
    }

    public function withCookies(array $cookies)
    {
        $this->cookies = $cookies;

        return $this;
    }

    public function withFiles(array $files)
    {
        $this->files = $files;

        return $this;
    }

    public function withServer(array $server)
    {
        $this->server = $server;

        return $this;
    }

    public function see($text)
    {
        PHPUnit::assertRegexp('/'.preg_quote($text, '/').'/i', $this->lastResponse->getContent());

        return $this;
    }

    public function assertResponseOk()
    {
        $actual = $this->lastResponse->getStatusCode();

        PHPUnit::assertSame(200, $actual, "Expected status code 200, got {$actual}.");
    }

    public function assertResponseStatus($code)
    {
        $actual = $this->lastResponse->getStatusCode();

        PHPUnit::assertSame($code, $actual, "Expected status code {$code}, got {$actual}.");
    }

    public function assertRedirectedTo($uri = null)
    {
        PHPUnit::assertTrue($this->lastResponse->isRedirect($uri));
    }

    public function assertRedirectedToRoute($name, $params = [])
    {
        $this->assertRedirectedTo($this->app->router->pathFor($name, $params));
    }

    protected function followRedirects()
    {
        while ($this->lastResponse->isRedirect()) {
            $this->browse($this->lastResponse->headers->get('location'));
        }

        return $this;
    }

    protected function clearInputs()
    {
        $this->currentRequest = null;
        $this->cookies = [];
        $this->files = [];
        $this->server = [];

        return $this;
    }

    protected function enableMiddleware()
    {
        $this->app->router->enableMiddleware();

        return $this;
    }
}
