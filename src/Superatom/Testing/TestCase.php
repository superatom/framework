<?php

namespace Superatom\Testing;

abstract class TestCase extends \PHPUnit_Framework_TestCase
{
    use CrawlerTrait;

    /**
     * @return \Superatom\Application
     */
    abstract public function createApplication();
}
