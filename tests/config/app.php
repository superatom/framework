<?php

return [
    'debug' => false,
    'timezone' => 'UTC',
    'log' => [
        'output' => 'php://stdout',
        'severity' => 'info',
    ],
    'templates' => base_path('app/views'),
];
