<?php

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class ControllerRegistrarTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @var \Superatom\Routing\ControllerRegistrar
     */
    protected $router;

    protected function setUp()
    {
        $basePath = __DIR__.'/../..';
        $app = new Superatom\Application($basePath);
        $resolver = new \Superatom\Routing\ControllerResolver($app);
        $this->router = new \Superatom\Routing\ControllerRegistrar();
        $this->router->setRouteResolver($resolver);
    }

    protected function tearDown()
    {
        $this->router = null;
    }

    public function testControllerMethodsRegister()
    {
        $names = [
            'getHello' => 'stub.hello',
            'postHey' => 'stub.hey',
            'getVariables' => 'stub.variables',
        ];
        $this->router->controller('StubController', $names);

        $this->assertResponseBody('hello', $this->dispatch('GET', '/stub/hello'));
        $this->assertSame('/stub/hello', $this->router->pathFor('stub.hello'));
        $this->assertResponseBody('hey', $this->dispatch('POST', '/stub/hey'));
        $this->assertSame('/stub/hey', $this->router->pathFor('stub.hey'));
        $this->assertResponseBody('got hoge and piyo', $this->dispatch('GET', '/stub/variables/hoge/piyo'));
        $vars = ['var1' => 'hoge', 'var2' => 'piyo'];
        $this->assertSame('/stub/variables/hoge/piyo', $this->router->pathFor('stub.variables', $vars));
    }

    public function testResourceMethodsRegister()
    {
        $this->router->resource('StubResourceController');

        $this->assertResponseBody('index', $this->dispatch('GET', '/stub_resource'));
        $this->assertSame('/stub_resource', $this->router->pathFor('stub_resource.index'));
        $this->assertResponseBody('create', $this->dispatch('GET', '/stub_resource/create'));
        $this->assertSame('/stub_resource/create', $this->router->pathFor('stub_resource.create'));
        $this->assertResponseBody('store', $this->dispatch('POST', '/stub_resource'));
        $this->assertSame('/stub_resource', $this->router->pathFor('stub_resource.store'));
        $this->assertResponseBody('show 1', $this->dispatch('GET', '/stub_resource/1'));
        $this->assertSame('/stub_resource/1', $this->router->pathFor('stub_resource.show', ['id' => 1]));
        $this->assertResponseBody('edit 1', $this->dispatch('GET', '/stub_resource/1/edit'));
        $this->assertSame('/stub_resource/1/edit', $this->router->pathFor('stub_resource.edit', ['id' => 1]));
        $this->assertResponseBody('update 1', $this->dispatch('PUT', '/stub_resource/1'));
        $this->assertSame('/stub_resource/1', $this->router->pathFor('stub_resource.update', ['id' => 1]));
        $this->assertResponseBody('destroy 1', $this->dispatch('DELETE', '/stub_resource/1'));
        $this->assertSame('/stub_resource/1', $this->router->pathFor('stub_resource.destroy', ['id' => 1]));
    }

    public function testResourceGroupsRegister()
    {
        $this->router->group('/test', function () {
            $this->router->resource('StubGroupController');
        });

        $this->assertSame('/test/stub_group', $this->router->pathFor('test.stub_group.index'));
    }

    protected function dispatch($method, $uri)
    {
        $req = Request::create($uri, $method);
        $res = new Response();

        return $this->router->dispatch($req, $res);
    }

    protected function assertResponseBody($expected, Response $res)
    {
        $this->assertTrue($res->isOk());
        $this->assertSame($expected, $res->getContent());
    }
}

class StubController extends \Superatom\Routing\Controller
{
    public function getHello()
    {
        return 'hello';
    }

    public function postHey()
    {
        return 'hey';
    }

    public function getVariables($var1, $var2)
    {
        return "got {$var1} and {$var2}";
    }
}

class StubResourceController extends \Superatom\Routing\Controller
{
    public function index()
    {
        return 'index';
    }

    public function create()
    {
        return 'create';
    }

    public function store()
    {
        return 'store';
    }

    public function show($id)
    {
        return 'show '.$id;
    }

    public function edit($id)
    {
        return 'edit '.$id;
    }

    public function update($id)
    {
        return 'update '.$id;
    }

    public function destroy($id)
    {
        return 'destroy '.$id;
    }
}

class StubGroupController extends \Superatom\Routing\Controller
{
    public function index()
    {
        return 'index';
    }
}
