<?php

namespace Superatom\Routing;

class ControllerFactoryTest extends \PHPUnit_Framework_TestCase
{
    public function test_resolve_no_args()
    {
        $class = 'Superatom\Routing\NoArgsController';
        $obj = ControllerFactory::create($class);

        $this->assertInstanceOf($class, $obj);
    }

    public function test_resolve_one_arg()
    {
        $class = 'Superatom\Routing\OneArgController';
        $obj = ControllerFactory::create($class);

        $this->assertInstanceOf($class, $obj);
    }

    public function test_resolve_two_args()
    {
        $class = 'Superatom\Routing\TwoArgsController';
        $obj = ControllerFactory::create($class);

        $this->assertInstanceOf($class, $obj);
    }

    public function test_resolve_recursive_dependencies()
    {
        $class = 'Superatom\Routing\RecursiveArgController';
        $obj = ControllerFactory::create($class);

        $this->assertInstanceOf($class, $obj);
    }

    public function test_resolve_recursive_dependencies_has_optional_param()
    {
        $class = 'Superatom\Routing\RecursiveArgHasOptionalParamController';
        $obj = ControllerFactory::create($class);

        $this->assertInstanceOf($class, $obj);
    }

    /**
     * @expectedException \InvalidArgumentException
     * @expectedExceptionMessage Superatom\Routing\CantResolveDepend $param
     */
    public function test_cant_resolve_dependencies()
    {
        $class = 'Superatom\Routing\CantResolveController';
        ControllerFactory::create($class);
    }
}

class Depend1
{
}
class Depend2
{
}

class NoArgsController
{
}

class OneArgController
{
    /**
     * @var Depend1
     */
    protected $dep1;

    /**
     * OneArgController constructor.
     *
     * @param Depend1 $dep1
     */
    public function __construct(Depend1 $dep1)
    {
        $this->dep1 = $dep1;
    }
}

class TwoArgsController
{
    /**
     * @var Depend1
     */
    protected $dep1;

    /**
     * @var Depend2
     */
    protected $dep2;

    /**
     * TwoArgsController constructor.
     *
     * @param Depend1 $dep1
     * @param Depend2 $dep2
     */
    public function __construct(Depend1 $dep1, Depend2 $dep2)
    {
        $this->dep1 = $dep1;
        $this->dep2 = $dep2;
    }
}

class Depend3
{
    /**
     * @var Depend1
     */
    protected $dep1;

    /**
     * Depend3 constructor.
     *
     * @param Depend1 $dep1
     */
    public function __construct(Depend1 $dep1)
    {
        $this->dep1 = $dep1;
    }
}

class RecursiveArgController
{
    /**
     * @var Depend3
     */
    protected $dep3;

    /**
     * RecursiveArgController constructor.
     *
     * @param Depend3 $dep3
     */
    public function __construct(Depend3 $dep3)
    {
        $this->dep3 = $dep3;
    }
}

class Depend4
{
    /**
     * @var Depend1
     */
    protected $dep1;

    /**
     * @var array
     */
    protected $arr;

    /**
     * Depend4 constructor.
     *
     * @param Depend1 $dep1
     * @param array $arr
     */
    public function __construct(Depend1 $dep1, array $arr = [])
    {
        $this->dep1 = $dep1;
        $this->arr = $arr;
    }
}

class RecursiveArgHasOptionalParamController
{
    /**
     * @var Depend4
     */
    protected $dep4;

    /**
     * RecursiveArgController constructor.
     *
     * @param Depend4 $dep4
     */
    public function __construct(Depend4 $dep4)
    {
        $this->dep4 = $dep4;
    }
}

class CantResolveDepend
{
    protected $param;

    /**
     * Required && Not class type hinting parameter
     *
     * @param $param
     */
    public function __construct($param)
    {
        $this->param = $param;
    }
}

class CantResolveController
{
    /**
     * @var CantResolveDepend
     */
    protected $cantDep;

    /**
     * CantResolveController constructor.
     * @param CantResolveDepend $cantDep
     */
    public function __construct(CantResolveDepend $cantDep)
    {
        $this->cantDep = $cantDep;
    }
}