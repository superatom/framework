<?php

class HelpersTest extends PHPUnit_Framework_TestCase
{
    /**
     * @var Superatom\Application
     */
    protected $app;

    /**
     * @var string
     */
    protected $basePath;

    protected function setUp()
    {
        $this->basePath = __DIR__.'/..';
        $this->app = new Superatom\Application($this->basePath);
    }

    protected function tearDown()
    {
        $this->app = null;
    }

    public function testAppPathFuncUsingInConfigFile()
    {
        $this->assertSame($this->basePath.'/app/views', $this->app->config->get('app.templates'));
    }

    public function testBasePathFunc()
    {
        $this->assertSame($this->basePath, base_path());
        $this->assertSame($this->basePath.'/app', base_path('app'));
        $this->assertSame($this->basePath.'/app/views', base_path('app/views'));
        $this->assertSame($this->basePath.'/app/views', base_path('/app/views/'));
    }

    public function testStoragePathFunc()
    {
        $this->assertSame($this->basePath.'/storage', storage_path());
        $this->assertSame($this->basePath.'/storage/cache', storage_path('cache'));
        $this->assertSame($this->basePath.'/storage/cache', storage_path('cache/'));
        $this->assertSame($this->basePath.'/storage/cache', storage_path('/cache/'));
    }
}
